var htmlparser = require('htmlparser');
var templatize = require('../lib/templatize.js');
var detemplatize = require('../lib/detemplatize.js');
var jsonToHtml = require('htmlparser-to-html');
var neowrapper = require('../lib/neowrapper.js');
var fileutils = require('../lib/fileutils.js');
var themeutils = require('../lib/themeutils.js');

module.exports = function(app, neo, express) {
	app.all('*', function(req, res, next) {
  		res.header("Access-Control-Allow-Origin", app.get('frontendUrl'));
  		res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  		res.header("Access-Control-Allow-Headers", "X-Requested-With");
  		res.header("Access-Control-Allow-Headers", "Content-Type");
  		res.header("Access-Control-Allow-Credentials", "true");
  		next();
 	});

    app.get('/', function(req, res){
        res.render('pages/dashboard', {});
    });

	app.get('/:themeid/:file', function(req, res) {
		Files.findOne({$and: [{'fileName': req.params.file}, {'themeId': req.params.themeid}]}).exec(function(err, file){
            if(err){
                res.status(202);
                res.set('Content-Type', 'text/html');
                res.end('<h1>SOMETHING WENT WRONG</h1>');
            }
            else if(!file){
                var filePath = './'+app.get('__CURRENT_FOLDER__')+'/'+req.params.themeid+'/'+req.params.file;
                fileutils.addFile(filePath, req.params.file, req.params.themeid, neo).then(function(){
                    Files.findOne({$and: [{'fileName': req.params.file}, {'themeId': req.params.themeid}]}).exec(function(err, file){
                        if(err){
                            console.log(err);
                        }
                        else{
                            neo.cypher({
                                query: 'MATCH (f)-[:FILE]->(n1), p=(n1)-[*]->(n2) WHERE f.fileId="'+file._id+'" RETURN n1, n2, p ORDER BY id(n2)'
                            }, 
                            function(err, graph){
                                if(err){
                                    console.log(err);
                                }
                                var htmlJson = detemplatize.fetch(graph);
                                var html = jsonToHtml(htmlJson);
                                res.status(200);
                                res.set('Content-Type', 'text/html');
                                res.end(html);
                            });
                        }
                    });
                },
                function(err){
                    res.status(202);
                    res.set('Content-Type', 'text/html');
                    res.end(err);
                });
            }
            else{
                neo.cypher({
                    query: 'MATCH (f)-[:FILE]->(n1), p=(n1)-[*]->(n2) WHERE f.fileId="'+file._id+'" RETURN n1, n2, p ORDER BY id(n2)'
                }, 
                function(err, graph){
                    if(err){
                        console.log(err);
                    }
                    var htmlJson = detemplatize.fetch(graph);
                    var html = jsonToHtml(htmlJson);
                    res.status(200);
                    res.set('Content-Type', 'text/html');
                    res.end(html);
                });    
            }
        });
    });

    app.get('/edit/:themeid/:file', function(req, res){
		res.render('pages/edit', {
			file: app.get('__DOMAIN__')+'/'+req.params.themeid+'/'+req.params.file,
            themeId: req.params.themeid
		});
	});

    
    app.get('/preview/:themeid/:file', function(req, res){
        res.render('pages/preview', {
            file: app.get('__DOMAIN__')+'/'+req.params.themeid+'/'+req.params.file,
            themeId: req.params.themeid
        });
    });

    app.post('/publish', function(req, res){
        themeutils.publishTheme(req.body.themeId, neo, app).then(function(data){
            console.log(data);
        }, function(err){
            console.log(err);
        });
    });
    
    app.post('/save', function(req, res){
        for(var i=0; i<req.body.edits.length; i++){
            var handler = new htmlparser.DefaultHandler(function (error) {
                if(error){
                    console.log(error);
                }
            }, 
            {
                verbose: false,
                ignoreWhitespace: true 
            });

            var parser = new htmlparser.Parser(handler);
            parser.parseComplete(req.body.edits[i].html);

            var jsonHtml = handler.dom;
            neowrapper.updateNode(neo, req.body.edits[i].id, jsonHtml[0], true);
            if(jsonHtml[0].children){
                for(var j=0; j<jsonHtml[0].children.length; j++){
                   neowrapper.addNode(neo, req.body.edits[i].id, jsonHtml[0].children[j]);
                }    
            }
        }
        
        res.set('Content-Type', 'application/json');
        res.end(JSON.stringify({'success': true}));
    });

    app.use(express.static(app.get('__CURRENT_FOLDER__')+'/'));
}