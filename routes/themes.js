var multer  = require('multer');
var themeutils = require('../lib/themeutils.js');

module.exports = function(app, neo, express) {
	var uploadedFile = {};
	
	// upload the file and move it to a directory in uploads
	app.use(multer({ dest: './tmp/',
 		rename: function (fieldname, filename) {
 	   		return filename+'_'+Date.now();
  		},
		onFileUploadStart: function (file) {
			console.log(file.originalname + ' is starting ...')
		},
		onFileUploadComplete: function (file) {
			uploadedFile = file;
		}
	}));

	/* Upload a new theme */

	app.post('/themes', function(req, res) {
		themeutils.addTheme(uploadedFile, app).then(function(data){
			if(data.success==true){
				res.status(200);
    			res.set('Content-Type', 'application/json');
    			res.end(JSON.stringify(data));
			}
		}, 
		function(err){
			if(err){
				res.status(200);
    			res.set('Content-Type', 'application/json');
    			res.end(JSON.stringify({
    				'success': false,
    				'reason': err
				}));	
			}
		});		
	});

	/* Upload an image */

	app.post('/images/:themeId', function(req, res) {
		themeutils.addImage(req.params.themeId, uploadedFile, app).then(function(data){
			res.status(200);
    		res.set('Content-Type', 'application/json');
    		res.end(JSON.stringify(data));
		}, function(err){
			res.status(200);
    		res.set('Content-Type', 'application/json');
    		res.end(JSON.stringify(err));
		});
	});
}
