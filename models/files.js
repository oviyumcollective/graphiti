var mongoose = require('mongoose');

var FilesSchema = mongoose.Schema({
	fileName: {
		type:'String',
		required: true
	},
	filePath:{
		type: 'String',
		required: true	
	},
	themeId: {
		type: 'String',
		required: true
	},
	createdOn:{
		type: 'Number',
		required: true
	},
	status: {
		type: 'Boolean',
		required: true
	}
});

module.exports = mongoose.model('files', FilesSchema);