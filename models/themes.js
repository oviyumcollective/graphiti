var mongoose = require('mongoose');

var ThemesSchema = mongoose.Schema({
	themeOriginalName: {
		type:'String',
		required: true
	},
	themeName:{
		type: 'String'
	},
	version: {
		type: 'Number',
		default: 0
	},
	createdOn:{
		type: 'Number',
		required: true
	},
	status: {
		type: 'Boolean',
		required: true
	}
});

module.exports = mongoose.model('themes', ThemesSchema);