var Q = require('q');
var fs = require('fs');
var htmlparser = require('htmlparser');
var async = require('async');

var nodeCount = 0;

function htmlToCypher(html, cypher, parentNode, rootNode){
    var currentNode;
    // check if its a fresh node
    if(!cypher){
        cypher = "";
    }
    // write the cypher
    cypher+='(n'+nodeCount+' ';
    if(html.type && html.type=='tag'){
        cypher+=': '+html.name+' ';
    }    
    var json = {};
    for(property in html){
        if(property!='children'){
            if(property=='attribs'){
                for(attribs in html[property]){
                    // replace all hyphens with underscores
                    attribs = attribs.replace(/-/g, '_');
                    json['attribs_'+attribs] = html[property][attribs];
                }
            }
            else{
                json[property] = html[property];    
            }
        }
    }
    json = JSON.stringify(json);
    json = json.replace(/\"([^(\")"]+)\":/g,"$1:");
    cypher+=json+'), ';
    currentNode = nodeCount;
    nodeCount++;

    if(rootNode){
        cypher+=' (f)-[:FILE]->(n'+(currentNode)+'), ';
    }

    if(parentNode){
        cypher+=' (n'+parentNode+')-[:CHILDREN]->(n'+(currentNode)+'), ';
    }

    if(html.hasOwnProperty('children')){
        for(var i=0; i<html.children.length; i++){
            cypher = htmlToCypher(html.children[i], cypher, (currentNode));
        }
    }

    return cypher;
}

var create = function(filename, fileId, themeId, neo){
    var deferred = Q.defer();
    fs.readFile(filename, 'utf8', function (err,data) {
        if (err) {
            return console.log(err);
        }
        var handler = new htmlparser.DefaultHandler(function (error) {
            if(error){
                console.log(error);
            }
        }, 
        {
            verbose: false,
            ignoreWhitespace: true 
        });

        var parser = new htmlparser.Parser(handler);
        parser.parseComplete(data);
        
        // create root node with fileId
        neo.cypher({
            query: 'CREATE (f:file {fileId: "'+fileId+'", themeId: "'+themeId+'"}) RETURN f'
        }, function(err, f){
            var fileId = f[0].f._id;
            var i = 0;
            async.eachSeries(handler.dom, function(h, nextLoop){
                var cypher = htmlToCypher(handler.dom[i], false, false, true);
                var lastComma = cypher.lastIndexOf(',');
                cypher = cypher.substring(0, lastComma);
                neo.cypher({
                    query: 'MATCH (f) WHERE id(f)='+fileId+' CREATE '+cypher
                }, 
                function(err, res){
                    if(err){
                        console.log(err);
                        return deferred.reject(err);
                    }
                    if(res.length==0){
                        console.log('Sucessfully templatized file '+filename);
                        i++;
                        nextLoop();
                    }
                });
            }, function(err){
                if(err){
                    console.log(err);
                }
                else{
                    return deferred.resolve();
                }
            });
        });
    });
    return deferred.promise;
};

module.exports = {
    create: create
}