function removeInvertedCommas (json) {
	// removes inverted commas from JSON stringify, makes it cypher compatiable
	json = JSON.stringify(json);
    json = json.replace(/\"([^(\")"]+)\":/g,"$1:")
    return json;
}

function attribsConversion (json){
	// removes children, makes the attributes cypher compatiable
	var jsonToReturn = {};
	for(property in json){
        if(property!='children'){
            if(property=='attribs'){
                for(attribs in json[property]){
                    // replace all hyphens with underscores
                    attribs = attribs.replace(/-/g, '_');
                    jsonToReturn['attribs_'+attribs] = json[property][attribs];
                }
            }
            else{
                jsonToReturn[property] = json[property];
            }
        }
    }
    return jsonToReturn;
}

function isHtmlFile (filename){
    var extensionStarts = filename.lastIndexOf('.');
    var extension = filename.substring(extensionStarts+1, filename.length);
    if(extension=='html' || extension=='htm'){
        return true;
    }
    else{
        return false;
    }
}

module.exports = {
	removeInvertedCommas: removeInvertedCommas,
	attribsConversion: attribsConversion,
    isHtmlFile: isHtmlFile
}