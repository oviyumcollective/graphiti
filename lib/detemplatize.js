var _ = require('underscore');

var json = [];

function addNode(nodeId, jsonToAdd, obj){
	if(obj.data_cmsone_id == nodeId){
		if(obj.children){
			for(property in jsonToAdd){
				if(property.split('_')[0]=='attribs'){
					var underscorePos = property.indexOf('_');
					var attr = property.substring(underscorePos+1, property.length);
					attr = attr.replace(/_/g, '-');
					if(jsonToAdd.attribs){
						jsonToAdd.attribs[attr] = jsonToAdd[property];
					}
					else{
						jsonToAdd.attribs = {};
						jsonToAdd.attribs[attr] = jsonToAdd[property];	
					}
					delete jsonToAdd[property];
				}
				if(property == 'data_cmsone_id'){
					if(jsonToAdd.attribs){
						jsonToAdd.attribs['data-cmsone-id'] = jsonToAdd.data_cmsone_id;
					}
					else{
						jsonToAdd.attribs = {};
						jsonToAdd.attribs['data-cmsone-id'] = jsonToAdd.data_cmsone_id;
					}
				}
			}
			obj.children.push(jsonToAdd);
			return true;
		}
		else{
			obj.children = [];
			for(property in jsonToAdd){
				if(property.split('_')[0]=='attribs'){
					var underscorePos = property.indexOf('_');
					var attr = property.substring(underscorePos+1, property.length);
					attr = attr.replace(/_/g, '-');
					if(jsonToAdd.attribs){
						jsonToAdd.attribs[attr] = jsonToAdd[property];
					}
					else{
						jsonToAdd.attribs = {};
						jsonToAdd.attribs[attr] = jsonToAdd[property];	
					}
					delete jsonToAdd[property];
				}
				if(property == 'data_cmsone_id'){
					if(jsonToAdd.attribs){
						jsonToAdd.attribs['data-cmsone-id'] = jsonToAdd.data_cmsone_id;
					}
					else{
						jsonToAdd.attribs = {};
						jsonToAdd.attribs['data-cmsone-id'] = jsonToAdd.data_cmsone_id;
					}
				}
			}
			obj.children.push(jsonToAdd);
			return true;
		}
	}
	else if(obj.children){
		for(var i=0; i<obj.children.length; i++){
			if(addNode(nodeId, jsonToAdd, obj.children[i])==true){
				break;
			}
		}
	}
}

var fetch = function(graph) {
	json = [];
	// add the first json
	var html = graph[0].n1.properties;
	html.data_cmsone_id = graph[0].n1._id;
	json.push(html);

	for(var i=0; i<graph.length; i++){
		var jsonToAdd = graph[i].n2.properties;
		jsonToAdd.data_cmsone_id = graph[i].n2._id;
		var nodeUrl = (graph[i].p.nodes[graph[i].p.nodes.length-2]).split('/');
		var nodeId = parseInt(nodeUrl[nodeUrl.length-1]);
		addNode(nodeId, jsonToAdd, json[0]);
		
	}

	return json;
};

module.exports = {
	fetch: fetch
}