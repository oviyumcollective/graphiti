var Q = require('q');
var templatize = require('./templatize.js');

function addFile (filePath, file, themeId, neo) {
	var deferred = Q.defer(); 
	
	var file = new Files({
		fileName: file,
		filePath: filePath,
		themeId: themeId,
		createdOn: Date.now(),
		status: true
	});

	file.save(function(err, file){
		if(err){
			console.log(err);
		}
		else{
			templatize.create(filePath, file._id, themeId, neo).then(function(){
				return deferred.resolve();
			}, function(err){
				console.log(err);
				return deferred.reject(err);
			});
		}
	});

	return deferred.promise;
}

module.exports = {
	addFile: addFile
}