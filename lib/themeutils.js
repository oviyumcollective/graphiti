var Q = require('q');
var async = require('async');
var fs = require('fs');
var AdmZip = require('adm-zip');
var fsextra = require('fs.extra');
var jsonToHtml = require('htmlparser-to-html');
var archiver = require('archiver');

var detemplatize = require('../lib/detemplatize.js');

function addTheme(file, app){
	var deferred = Q.defer();
	var jsonToSend = {};
	// check the mime of file
	async.waterfall([
		function(doneMimeCheck){
			if(file.mimetype!='application/zip'){
				doneMimeCheck('wrongmime');
				fs.unlink(file.path, function(){});
			}
			else{
				doneMimeCheck(null);
			}
		},
		function(doneIndexCheck){
			var tmpzip = new AdmZip(file.path);
			var tmpFiles = tmpzip.getEntries();
			var indexFound = false;
			for(var i=0; i<tmpFiles.length; i++){
				if(tmpFiles[i].entryName=='index.htm' || tmpFiles[i].entryName=='index.html'){
					indexFound = true;
					if (tmpFiles[i].entryName=='index.htm') {
						jsonToSend.indexFile = 'index.htm';
					}
					else{
						jsonToSend.indexFile = 'index.html';	
					}
				}
			}
			if(indexFound==false){
				doneIndexCheck('noindexfile');
				fs.unlink(file.path, function(){});
			}
			else{
				doneIndexCheck(null);	
			}
		},
		function(doneDbEntry){
			var theme = new Themes({
				themeOriginalName: file.originalname.substring(0, file.originalname.lastIndexOf('.')),
				createdOn: Date.now(),
				status: true
			});

			theme.save(function(err, theme){
				if(err){
					console.log(err);
				}
				else{
					jsonToSend.themeId = theme._id;
					doneDbEntry(null, theme._id);
				}
			});
		},
		function(themeId, doneMkdir){
			async.waterfall([
				function(doneMkdirThemes){
					fs.mkdir('./'+app.get('__CURRENT_FOLDER__')+'/'+themeId, function(err){
						if(err){
							console.log(err);
						}
						else{
							doneMkdirThemes(null);
						}
					});
				},
				function(doneMkdirHistory){
					fs.mkdir('./'+app.get('__HISTORY_FOLDER__')+'/'+themeId, function(err){
						if(err){
							console.log(err);
						}
						doneMkdirHistory(null);
					});
				}
			], function(err){
				if(err){
					console.log(err);
				}
				else{
					doneMkdir(null, themeId);
				}
			});
		},
		function(themeId, doneMove){
			var filePath = './uploads/'+themeId+'.'+file.extension;
			var filePathHistory = './'+app.get('__HISTORY_FOLDER__')+'/'+themeId+'/'+themeId+'_initial_'+Date.now()+'.'+file.extension;
			fsextra.copy(file.path, filePath, function(err){
				if(err){
					console.log(err);
				}
				fsextra.copy(file.path, filePathHistory, function(err){
					if(err){
						console.log(err);
					}
					doneMove(null, themeId);
				});
			});		
		},
		function(themeId, doneExtraction){
			var zip = new AdmZip(file.path);
			zip.extractAllTo('./'+app.get('__CURRENT_FOLDER__')+'/'+themeId+'/', true);
			doneExtraction(null, themeId);
		},
		function(themeId, doneCopyAssets){
			fsextra.copyRecursive('./assets', './'+app.get('__CURRENT_FOLDER__')+'/'+themeId, function(err){
				if(err){
					console.log(err);
				}
				doneCopyAssets(null);
			});
		},
		function(doneDeleteTemp){
			fs.unlink(file.path, function(){});
			doneDeleteTemp(null);
		}
	], function(err){
		if(err){
			deferred.reject(err);
		}
		else{
			jsonToSend.success = true;
			deferred.resolve(jsonToSend);
		}
	});

	return deferred.promise;
}

function publishTheme(themeId, neo, app){
	var deferred = Q.defer();

	async.waterfall([
		function(doneTmpMkdir){
			fs.mkdir('./tmp/'+themeId, function(err){
				if(err){
					console.log(err);
				}
				else{
					doneTmpMkdir(null);
				}
			});
		},
		function(doneCopy){
			fsextra.copyRecursive('./'+app.get('__CURRENT_FOLDER__')+'/'+themeId, './tmp/'+themeId, function(err){
				if(err){
					console.log(err);
				}
				else{
					doneCopy(null);
				}
			});
		},
		function(doneDeleteGraphiti){
			fsextra.rmrf('./tmp/'+themeId+'/graphiti', function (err) {
				if(err){
					console.log(err);
				}
				else{
					doneDeleteGraphiti(null);
				}
			});
		},
		function(doneFindingFiles){
			Files.find({'themeId': themeId}).exec(function(err, files){
				if(err){
					console.log(err);
				}
				else{
					async.eachSeries(files, function(file, nextFile){
						async.waterfall([function(doneNeoFetch){
							neo.cypher({
                                query: 'MATCH (f)-[:FILE]->(n1), p=(n1)-[*]->(n2) WHERE f.fileId="'+file._id+'" RETURN n1, n2, p ORDER BY id(n2)'
                            }, 
                            function(err, graph){
                                if(err){
                                    console.log(err);
                                }
                                doneNeoFetch(null, graph);
                            });
						},
						function(graph, doneDetemplatize){
							var htmlJson = detemplatize.fetch(graph);
                            var html = jsonToHtml(htmlJson);
                            doneDetemplatize(null, html);
						},
						function(html, doneFileWrite){
							fs.unlink('./tmp/'+themeId+'/'+file.fileName, function(err){
								if(err){
									console.log(err);
								}
								else{
									fs.writeFile('./tmp/'+themeId+'/'+file.fileName, function(err){
										if(err){
											console.log(err);
										}
										else{
											doneFileWrite(null);
										}
									});
								}
							});
						}], function(err){
							if(err){
								console.log(err)
							}
							else{
								nextFile(null);
							}
						});
					}, function(err){
						if(err){
							console.log(err);
						}
						else{
							Themes.findOne({'_id':themeId}).exec(function(err, theme){
								if(err){
									console.log(err);
								}
								else{
									theme.version++;
									theme.save(function(err, theme){
										if(err){
											console.log(err);
										}
										else{
											doneFindingFiles(null, theme.version);
										}
									});
								}
							})
						}
					});
				}
			});
		},
		function(themeVersion, doneZippingFiles){
			var zipFile = './'+app.get('__HISTORY_FOLDER__')+'/'+themeId+'/'+themeId+'_'+themeVersion+'_'+Date.now()+'.zip';
			var outputZip = fs.createWriteStream(zipFile);
			var archive = archiver('zip');

			archive.on('error', function(err) {
  				console.log(err);
			});

			archive.pipe(outputZip);

			archive.directory('./tmp/'+themeId, false, { date: new Date() });

			archive.finalize();
			outputZip.on('close', function() {
  				doneZippingFiles(null, zipFile);
			});
			
		},
		function(zipFile, doneDeleteTmp){
			fsextra.rmrf('./tmp/'+themeId, function (err) {
  				if (err) {
    				console.error(err);
  				}
  				else{
  					doneDeleteTmp(null, zipFile);
  				}
			});
		}
	], function(err, zipFile){
		if(err){
			console.log(err);
			deferred.reject(err);
		}
		else{
			deferred.resolve(zipFile);
		}
	});

	return deferred.promise;
}

function addImage(themeId, file, app){
	var deferred = Q.defer();

	var jsonToSend = {};
	// check the mime of file
	async.waterfall([
		function(doneMimeCheck){
			if(file.mimetype=='image/jpeg' || file.mimetype=='image/png' || file.mimetype=='image/gif'){
				doneMimeCheck(null);
			}
			else{
				doneMimeCheck('wrongmime');
				fs.unlink(file.path, function(){});	
			}
		},
		function(checkDirExists){
			var filePath = './'+app.get('__CURRENT_FOLDER__')+'/'+themeId+'/img';

			fs.mkdir(filePath, function(err){
				if(err){
					if(err.code=='EEXIST'){
						checkDirExists(null, filePath);
					}
					else{
						console.log(err);
					}
				}
				else{
					checkDirExists(null, filePath);		
				}
			});
		},
		function(filePath, doneMoveFile){
			fsextra.move(file.path, filePath+'/'+file.name, function(err){
				if(err){
					console.log(err);
				}
				else{
					jsonToSend.filePath = 'img/'+file.name;
					jsonToSend.success = true;
					deferred.resolve(jsonToSend);
					doneMoveFile(null);	
				}
			});	
		}
	], function(err){
		if(err){
			jsonToSend.success = false;
			jsonToSend.err = err
			deferred.reject(jsonToSend);	
		}
	});

	return deferred.promise;
}

module.exports = {
	addTheme: addTheme,
	publishTheme: publishTheme,
	addImage: addImage
}