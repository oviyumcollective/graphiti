var utils = require('./utilities.js');

function updateNode (neo, nodeId, json, deleteChildern){
	json = utils.attribsConversion(json);
	var updateStr = '';
	for(var p in json){
		updateStr+='n.'+p+'="'+json[p]+'", '
	}
	var lastComma = updateStr.lastIndexOf(',');
    updateStr = updateStr.substring(0, lastComma);

	neo.cypher({
        query: 'MATCH (n) WHERE id(n)='+nodeId+' SET '+updateStr
    },
    function(err, res){
        if(err){
            console.log(err);
        }
		console.log(JSON.stringify(res));
    });

    if(deleteChildern){
    	neo.cypher({
            query: 'MATCH (n1)-[r*]->(n2) WHERE id(n1)='+nodeId+' DELETE n2 FOREACH (rel in r | DELETE rel)'
        }, 
        function(err, res){
            if(err){
                console.log(err);
            }
            console.log(JSON.stringify(res));
        });
    }
}

function addNode(neo, parentNodeId, json){
	json = utils.attribsConversion(json);
	json = utils.removeInvertedCommas(json);
	neo.cypher({
        query: 'MATCH n WHERE id(n)='+parentNodeId+' CREATE (n2 '+json+'), (n)-[:CHILDREN]->(n2)'
    },
    function(err, res){
        if(err){
            console.log(err);
        }
		console.log(JSON.stringify(res));
    });	
}

module.exports = {
	updateNode: updateNode,
	addNode: addNode
}