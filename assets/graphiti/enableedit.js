var editStack = [];

function isTagAllowed(tagname){
    var unAllowedTags = ['DIV','BODY'];
    for(var i=0; i<unAllowedTags.length; i++){
        if(tagname == unAllowedTags[i]){
            return false;
        }
    }
    return true;
}

var template = document.getElementById('editframe');

function getAllElementsWithAttribute(data_attribute_name, data_attribute_value){
    var matchingElements;
    var allElements = template.contentWindow.document.getElementsByTagName('*');
    for (var i = 0, n = allElements.length; i < n; i++){
        if (allElements[i].getAttribute(data_attribute_name) == data_attribute_value){
            matchingElements=allElements[i];
        }
    }
    return matchingElements;
}

template.addEventListener("load", function(){
    var editing = false;
    var editingNode;
    var editingNodeHtml;
    var editingNodeText; 

    template.contentDocument.addEventListener('click', function(e){
        e.preventDefault();
        e.stopPropagation();
        if(e.target.innerText && !editing){
            if(isTagAllowed(e.target.tagName)){
                editStack.push(e.target.getAttribute("data-cmsone-id"));
                // check if clicked element is a link
                if(e.target.tagName == 'A'){
                    bootbox.dialog({
                        title: "Edit Link",
                        message: '<div class="row">  ' +
                                    '<div class="col-md-12"> ' +
                                        '<form class="form-horizontal"> ' +
                                            '<div class="form-group"> ' +
                                                '<label class="col-md-4 control-label">Text</label> ' +
                                                '<div class="col-md-4"> ' +
                                                    '<input id="linktext" type="text" class="form-control input-md" value="'+e.target.innerText+'" width="auto"> ' +
                                                '</div> '+
                                            '</div>'+
                                            '<div class="form-group"> ' +
                                                '<label class="col-md-4 control-label">Link</label> ' +
                                                '<div class="col-md-4"> ' +
                                                    '<input id="linklink" type="text" class="form-control input-md" value="'+e.target.href+'" width="auto"> ' +
                                                '</div> '+
                                            '</div>'+
                                        '</form>'+
                                    '</div>'+
                                '</div>',
                        buttons: {
                            success: {
                                label: "Save",
                                className: "btn-success",
                                callback: function () {
                                    var linktext = document.getElementById('linktext').value;
                                    var linklink = document.getElementById('linklink').value;
                                    e.target.href = linklink;
                                    e.target.innerText = linktext;
                                    console.log(e.target);
                                }
                            }
                        }
                    });
                }
                else{
                    editing = true;
                    editingNode = e.target;
                    editingNodeHtml = e.target.innerHTML;
                    e.target.innerHTML = "<textarea id='editbox'>"+e.target.innerHTML+"</textarea>";
               
                    template.contentDocument.getElementById('editbox').addEventListener('click', function(etextbox){
                        etextbox.preventDefault();
                        etextbox.stopPropagation();
                    });        
                }
            }
        }
        else if(e.target.tagName=='IMG' && !editing){
            editStack.push(e.target.getAttribute("data-cmsone-id"));
            bootbox.dialog({
                title: "Upload Image",
                message: '<div class="row">  ' +
                            '<div class="col-md-12"> ' +
                                '<form class="form-horizontal" enctype="multipart/form-data" id="cmsuploadform"> ' +
                                    '<div class="form-group"> ' +
                                        '<label class="col-md-4 control-label">File Button</label>' +
                                            '<div class="col-md-4">' +
                                                '<input id="cmsonfileupload" name="cmsonfileupload" class="input-file" type="file">' +
                                            '</div>' +
                                    '</div>'+
                                '</form>'+
                            '</div>'+
                        '</div>',
                buttons: {
                    success: {
                        label: "Done",
                        className: "btn-success",
                        callback: function () {
                            var formData = new FormData($('#cmsuploadform')[0]);
                            var themeId = window.location.href.split('/')[4];
                            $.ajax({
                                url: '/images/'+themeId,
                                type: 'POST',
                                xhr: function() {  // Custom XMLHttpRequest
                                    var myXhr = $.ajaxSettings.xhr();
                                    if(myXhr.upload){ // Check if upload property exists
                                        
                                    }
                                    return myXhr;
                                },
                                //Ajax events
                                success: function(data){
                                    if(data.success==true){
                                        e.target.src = data.filePath;
                                    }
                                    else{
                                        toastr.error('Check the file!');    
                                    }
                                },
                                error: function(err){
                                    toastr.error('Check the file!');
                                },
                                // Form data
                                data: formData,
                                //Options to tell jQuery not to process data or worry about content-type.
                                cache: false,
                                contentType: false,
                                processData: false
                            });
                        }
                    }
                }
            });
        }
        else if(editing == true){
            editingNodeText = template.contentDocument.getElementById('editbox').value;
            editingNode.innerHTML = editingNodeHtml;
            // editingNode.innerText = editingNodeText;
            editing = false;
        }
    });
}, false);

document.getElementById('cmsonsave').addEventListener('click', function(){
    var uniqueEditStack = _.uniq(editStack);

    var jsonToSend = {};
    jsonToSend.edits = [];

    for(var i=0; i<uniqueEditStack.length; i++){
        if(uniqueEditStack[i]){
            jsonToSend.edits.push({
                'id': uniqueEditStack[i],
                'html': getAllElementsWithAttribute('data-cmsone-id', uniqueEditStack[i].toString()).outerHTML
            });    
        }
    }

    var http = new XMLHttpRequest();
    var url = "/save";
    http.open("POST", url, true);
    http.setRequestHeader("Content-type", "application/json");
    http.onreadystatechange = function() {
        if(http.readyState == 4 && http.status == 200) {
            console.log(http.responseText);
            alert("Changes Saved");            
        }
    }
    http.send(JSON.stringify(jsonToSend));
    
});