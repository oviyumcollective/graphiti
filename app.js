var express = require('express');
var neo4j = require('neo4j');
var mongoose = require('mongoose');
var bodyparser = require('body-parser');
var fs = require('fs');

var utilities = require('./lib/utilities.js');
var fileutils = require('./lib/fileutils.js');

var app = express();

/* Include the configurations */

require('./config/environment.js')(app, express, bodyparser);
require('./config/credentials.js')(app, mongoose, neo4j);

// connect to neo4j
var neo = new neo4j.GraphDatabase(app.get('neo4j'));

/* Include the models */
Files = require('./models/files.js');
Themes = require('./models/themes.js');

/* Include the routes */

// main route
require('./routes/index.js')(app, neo, express);
require('./routes/themes.js')(app, neo, express);

switch(process.argv[2]){
    case 'create':
    	// read all the files in the given directory
    	fs.readdir('./'+process.argv[3], function(err, files){
    		for(var i=0; i<files.length; i++){
    			if(utilities.isHtmlFile(files[i])){
    				fileutils.addFile('./'+process.argv[3]+files[i], files[i], neo);
    			}
    		}
    	});
        break;
}

app.listen(app.get('port'), function(){
    console.log("Express server listening on port %d", app.get('port'));
});