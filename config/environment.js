module.exports = function(app, express, bodyparser){
    app.use(bodyparser.urlencoded({
        extended: false
    }));

    app.use(bodyparser.json());

    app.set('view engine', 'ejs');

    app.set('__CURRENT_FOLDER__', 'sites/current');
    app.set('__HISTORY_FOLDER__', 'sites/history');
	app.set('__DOMAIN__', 'http://localhost:5000');
    app.set('port', 5000);
};
