module.exports = function(app, mongoose, neo4j){
	app.set('db', 'mongodb://localhost:27017/graphiti');
	app.set('neo4j', 'http://localhost:7474');

	// connect to mongo
	mongoose.connect(app.get('db'));

	var db = mongoose.connection;
	db.on('error', console.error.bind(console, 'connection error:'));
	db.once('open', function callback () {
		console.log('db connected');
	});
}